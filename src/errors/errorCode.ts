export enum ErrorCode {
  unauthenticated = "UNAUTHENTICATED",
  graphQLValidation = "GRAPHQL_VALIDATION_FAILED",
  userInputError = "USER_INPUT_ERROR",
  notFound = "NOT_FOUND",
  placeId = "PLACE_ID",
  scopeError = "SCOPE_ERROR",
  wtf = "WTF",
  notImplemented = "NOT_IMPLEMENTED",
  logicError = "LOGIC_ERROR",
  environmentError = "ENVIRONMENT_ERROR"
}

export class BHError extends Error {
  constructor(public readonly code: ErrorCode, message: string) {
    super(message);
  }
}
