// tslint:disable: no-magic-numbers

export const bytesToSize = (bytes: number, seperator = '') => {
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes === 0) {
    return '';
  }
  const i = Math.floor(Math.log(bytes) / Math.log(1024));
  if (i === 0) {
    return `${bytes}${seperator}${sizes[i]}`;
  }
  return `${(bytes / 1024 ** i).toFixed(1)}${seperator}${sizes[i]}`;
};
