export function isValidSlug(slug: string): boolean {
  return validSlugRegExp.test(slug);
}

const validSlugRegExp = /^[A-Za-z0-9$\-_.+!*'(),]+$/;
