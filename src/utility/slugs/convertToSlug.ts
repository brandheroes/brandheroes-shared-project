export function convertToSlug(text: string): string {
  const trimmedInput = text.trim();
  let slug = '';
  for (let i = 0; i < trimmedInput.length; i++) {
    const char = trimmedInput[i];
    if (validChar.test(char) && char !== '-') {
      slug += char;
    } else if ((whitespace.test(char) || char === '-') && slug[slug.length - 1] !== '-') {
      slug += '-';
    }
  }
  return slug;
}

const validChar = /^[A-Za-z0-9$\-_.+!*'(),]$/;
const whitespace = /^\s$/;
