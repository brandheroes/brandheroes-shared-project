import { isValidSlug } from '../isValidSlug';
import { convertToSlug } from '../convertToSlug';

describe('slug tests', () => {
  describe('correctly validates some test slugs', () => {
    it('rejects non slugified strings', () => {
      const slug = 'A Campaign With Spaces';
      const slug2 = 'ACampaignWith$%^1signs';
      expect(isValidSlug(slug)).toBe(false);
      expect(isValidSlug(slug2)).toBe(false);
    });

    it('rejects non slugified strings', () => {
      const slug = 'A Campaign With Spaces';
      expect(isValidSlug(slug)).toBe(false);
    });
  });

  describe('correctly converts strings to slugs', () => {
    it('converts spaces', () => {
      const slug = 'A Campaign With Spaces';
      expect(convertToSlug(slug)).toMatch('A-Campaign-With-Spaces');
    });

    it('Strips funky characters', () => {
      const slug = 'A Campaign With $1gn$!±§§12!';
      expect(convertToSlug(slug)).toMatch('A-Campaign-With-$1gn$!12!');
    });
  });
});
