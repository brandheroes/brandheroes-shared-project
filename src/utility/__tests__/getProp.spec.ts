import { getProp } from '../getProp';

describe('getProp', () => {
  test('it gets a property at the first level', () => {
    interface A {
      bar: number;
      baz: string;
    }
    const a: A = {
      bar: 0,
      baz: '',
    };
    expect(
      getProp(a)
        .on('bar')
        .get()
    ).toBe(0);
  });
  test('it gets a property at the second level', () => {
    interface A {
      bar: number;
      baz: {
        foo: string;
        bla: number;
      };
    }
    const a: A = {
      bar: 0,
      baz: { foo: '', bla: 0 },
    };
    expect(
      getProp(a)
        .on('baz')
        .on('foo')
        .get()
    ).toBe('');
  });
  test('it gets a property at the third level', () => {
    interface A {
      bar: number;
      baz: {
        foo: string;
        bla?: {
          got: number;
          dot: string;
        };
      };
    }
    const a: A = {
      bar: 0,
      baz: { foo: '', bla: { dot: '', got: 1 } },
    };
    expect(
      getProp(a)
        .on('baz')
        .on('bla')
        .on('got')
        .get()
    ).toBe(1);
  });
});

describe('getProp onValue', () => {
  test('should get the property at the first level', () => {
    const a = {
      foo: {
        value: 'test',
      },
    };

    expect(
      getProp(a)
        .onValue('foo')
        .get()
    ).toBe('test');
  });

  test('should get the property at the first level with id', () => {
    const a = {
      id: '',
      foo: {
        value: 'test',
      },
    };

    expect(
      getProp(a)
        .onValue('foo')
        .get()
    ).toBe('test');
  });

  test('should get the property at the second level', () => {
    const a = {
      foo: {
        value: { bar: { value: 'test' } },
      },
    };

    expect(
      getProp(a)
        .onValue('foo')
        .onValue('bar')
        .get()
    ).toBe('test');
  });

  test('should get the property at the second level with id', () => {
    const a = {
      id: '',
      foo: {
        value: { id: '', bar: { value: 'test' } },
      },
    };

    expect(
      getProp(a)
        .onValue('foo')
        .onValue('bar')
        .get()
    ).toBe('test');
  });
});
