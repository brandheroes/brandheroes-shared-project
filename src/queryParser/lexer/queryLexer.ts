import { QueryToken } from '../tokenizer/queryToken';
import { ASTNode, cases, follows, just, LexResult, numberLiteral, RangeASTNode, stringLiteral, tag } from './lexer';

function andSet(nodes: ASTNode[]): ASTNode {
  return nodes.length <= 0
    ? { kind: 'BOOLEAN', value: true }
    : nodes.length === 1
      ? nodes[0]
      : nodes.reduce((left, right) => ({ kind: 'AND', left, right }));
}

function orSet(nodes: ASTNode[]): ASTNode {
  return nodes.length <= 0
    ? { kind: 'BOOLEAN', value: true }
    : nodes.length === 1
      ? nodes[0]
      : nodes.reduce((left, right) => ({ kind: 'OR', left, right }));
}

function propertyAST(nodes: ASTNode[]): ASTNode {
  // tslint:disable-next-line:no-magic-numbers
  if (nodes.length !== 2) {
    throw new Error('Invalid node count in translation case');
  }
  const firstNode = nodes[0];
  const secondNode = nodes[1];
  if (
    firstNode.kind !== 'STRING' ||
    !(secondNode.kind === 'STRING' || secondNode.kind === 'NUMBER' || secondNode.kind === 'RANGE')
  ) {
    throw new Error('Invalid node type in translation case');
  }
  return { kind: 'PROPERTY', name: firstNode, value: secondNode };
}

function minMaxRangeAST(nodes: ASTNode[]): RangeASTNode {
  // tslint:disable-next-line:no-magic-numbers
  if (nodes.length !== 2) {
    throw new Error('Invalid node count in translation case');
  }
  const min = nodes[0];
  const max = nodes[1];
  if (min.kind !== 'NUMBER' || max.kind !== 'NUMBER') {
    throw new Error('Invalid node type in translation case');
  }
  return { kind: 'RANGE', min, max };
}

function minRangeAST(nodes: ASTNode[]): RangeASTNode {
  if (nodes.length !== 1) {
    throw new Error('Invalid node count in translation case');
  }
  const min = nodes[0];
  if (min.kind !== 'NUMBER') {
    throw new Error('Invalid node type in translation case');
  }
  return { kind: 'RANGE', min };
}

function maxRangeAST(nodes: ASTNode[]): RangeASTNode {
  if (nodes.length !== 1) {
    throw new Error('Invalid node count in translation case');
  }
  const max = nodes[0];
  if (max.kind !== 'NUMBER') {
    throw new Error('Invalid node type in translation case');
  }
  return { kind: 'RANGE', max };
}

export function lexQuery(queryTokens: QueryToken[]) {
  const prop = (tokens: QueryToken[]): LexResult =>
    cases(
      follows(andSet, just('('), lex, just(')')), // Group
      follows(
        propertyAST,
        tag(),
        just(':'),
        cases(
          follows(minMaxRangeAST, numberLiteral(), just('-'), numberLiteral()),
          follows(minRangeAST, numberLiteral(), just('-')),
          follows(maxRangeAST, just('-'), numberLiteral()),
          numberLiteral(),
          stringLiteral()
        )
      ),
      stringLiteral()
    )(tokens);
  const lex = (tokens: QueryToken[]): LexResult =>
    cases(
      prop,
      follows(andSet, prop, lex),
      follows(andSet, prop, just('&'), lex),
      follows(orSet, prop, just('|'), lex)
    )(tokens);
  const result = lex(queryTokens);
  return result && result.tokensConsumed === queryTokens.length && result.astNode;
}
