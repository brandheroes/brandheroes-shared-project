import { QueryToken } from '../tokenizer/queryToken';

export type PositiveLexResult = { tokensConsumed: number; astNode?: ASTNode };

export type LexResult = false | PositiveLexResult;

export type ASTNode =
  | AndASTNode
  | OrASTNode
  | PropertyASTNode
  | BooleanASTNode
  | NumberASTNode
  | RangeASTNode
  | StringASTNode;

export type AndASTNode = {
  kind: 'AND';
  left: ASTNode;
  right: ASTNode;
};

export type OrASTNode = {
  kind: 'OR';
  left: ASTNode;
  right: ASTNode;
};

export type PropertyASTNode = {
  kind: 'PROPERTY';
  name: StringASTNode;
  value: StringASTNode | NumberASTNode | RangeASTNode;
};

export type BooleanASTNode = {
  kind: 'BOOLEAN';
  value: boolean;
};

export type NumberASTNode = {
  kind: 'NUMBER';
  value: number;
};

export type RangeASTNode = {
  kind: 'RANGE';
  min?: NumberASTNode;
  max?: NumberASTNode;
};

export type StringASTNode = {
  kind: 'STRING';
  value: string;
};

export type LexFunctionResult = (tokens: QueryToken[]) => LexResult;

export function just(target: string): LexFunctionResult {
  return tokens => (tokens.length >= 1 && tokens[0].value === target ? { tokensConsumed: 1 } : false);
}

export function tag(): LexFunctionResult {
  return tokens =>
    tokens.length >= 1 && /^\w+$/.test(tokens[0].value)
      ? { tokensConsumed: 1, astNode: { kind: 'STRING', value: tokens[0].value } }
      : false;
}

export function numberLiteral(): LexFunctionResult {
  return tokens =>
    tokens.length >= 1 &&
    /^[0-9]+(\.[0-9]+)?$/.test(tokens[0].value) && {
      tokensConsumed: 1,
      astNode: { kind: 'NUMBER', value: parseFloat(tokens[0].value) },
    };
}

export function stringLiteral(): LexFunctionResult {
  return tokens =>
    tokens.length >= 1 &&
    /^([^\s"]*|"(?:[^"\\]|\\.)*")$/.test(tokens[0].value) && {
      tokensConsumed: 1,
      astNode: {
        kind: 'STRING',
        value: tokens[0].value.startsWith('"') ? tokens[0].value.slice(1, tokens[0].value.length - 1) : tokens[0].value,
      },
    };
}

export function cases(...patterns: LexFunctionResult[]): LexFunctionResult {
  return tokens => {
    const matches = patterns
      .map(pattern => pattern(tokens))
      .filter((match): match is PositiveLexResult => match !== false);

    if (matches.length <= 0) {
      return false;
    } else {
      return matches.length === 1
        ? matches[0]
        : matches.reduce((a, b) => (a.tokensConsumed > b.tokensConsumed ? a : b));
    }
  };
}

export function follows(
  transformer: (nodes: ASTNode[]) => ASTNode,
  ...patterns: LexFunctionResult[]
): LexFunctionResult {
  return tokens => {
    let matchLength = 0;
    let remaining = tokens;
    const nodes: ASTNode[] = [];

    for (const pattern of patterns) {
      const matched = pattern(remaining);

      if (matched === false) {
        return false;
      }
      if (matched.astNode !== undefined) {
        nodes.push(matched.astNode);
      }
      matchLength = matchLength + matched.tokensConsumed;
      remaining = tokens.slice(matchLength);
    }

    return { tokensConsumed: matchLength, astNode: transformer(nodes) };
  };
}

export function zeroOrMore(pattern: LexFunctionResult, transformer: (nodes: ASTNode[]) => ASTNode): LexFunctionResult {
  return tokens => {
    let matchedLength = 0;
    let remaining = tokens;
    const nodes: ASTNode[] = [];

    while (remaining.length > 0) {
      const matched = pattern(remaining);

      if (matched === false || matched.tokensConsumed === 0) {
        break;
      }
      if (matched.astNode !== undefined) {
        nodes.push(matched.astNode);
      }

      matchedLength = matchedLength + matched.tokensConsumed;
      remaining = remaining.slice(matched.tokensConsumed);
    }

    return { tokensConsumed: matchedLength, astNode: transformer(nodes) };
  };
}

export function oneOrMore(pattern: LexFunctionResult, transformer: (nodes: ASTNode[]) => ASTNode): LexFunctionResult {
  return tokens => {
    let matchedLength = 0;
    let remaining = tokens;
    const nodes: ASTNode[] = [];

    while (remaining.length > 0) {
      const matched = pattern(remaining);

      if (matched === false || matched.tokensConsumed === 0) {
        break;
      }
      if (matched.astNode) {
        nodes.push(matched.astNode);
      }

      matchedLength = matchedLength + matched.tokensConsumed;
      remaining = remaining.slice(matched.tokensConsumed);
    }

    return matchedLength > 0 && { tokensConsumed: matchedLength, astNode: transformer(nodes) };
  };
}
