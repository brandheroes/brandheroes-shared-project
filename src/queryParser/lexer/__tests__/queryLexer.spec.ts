import { lexQuery } from '../queryLexer';

describe('lexQuery', () => {
  test('It can lex a property', () => {
    expect(
      lexQuery([
        { start: 0, end: 3, value: 'age' },
        { start: 3, end: 4, value: ':' },
        { start: 4, end: 5, value: '1' },
        { start: 5, end: 6, value: '-' },
      ])
    ).toEqual({
      kind: 'PROPERTY',
      name: { kind: 'STRING', value: 'age' },
      value: { kind: 'RANGE', min: { kind: 'NUMBER', value: 1 } },
    });
  });

  test('It can lex a property followed by a loose string', () => {
    expect(
      lexQuery([
        { start: 0, end: 1, value: 'r' },
        { start: 1, end: 2, value: ':' },
        { start: 2, end: 3, value: '1' },
        { start: 4, end: 5, value: 'a' },
      ])
    ).toEqual({
      kind: 'AND',
      left: { kind: 'PROPERTY', name: { kind: 'STRING', value: 'r' }, value: { kind: 'STRING', value: '1' } },
      right: { kind: 'STRING', value: 'a' },
    });
  });

  test('It can lex a loose string followed by a property followed by a loose string', () => {
    expect(
      lexQuery([
        { start: 0, end: 1, value: 'a' },
        { start: 2, end: 3, value: 'b' },
        { start: 3, end: 4, value: ':' },
        { start: 4, end: 5, value: '1' },
        { start: 6, end: 7, value: 'b' },
      ])
    ).toEqual({
      kind: 'AND',
      left: { kind: 'STRING', value: 'a' },
      right: {
        kind: 'AND',
        left: { kind: 'PROPERTY', name: { kind: 'STRING', value: 'b' }, value: { kind: 'STRING', value: '1' } },
        right: { kind: 'STRING', value: 'b' },
      },
    });
  });

  test('It can lex a tag with an empty string literal value', () => {
    expect(
      lexQuery([{ start: 0, end: 1, value: 'a' }, { start: 1, end: 2, value: ':' }, { start: 2, end: 2, value: '' }])
    ).toEqual({ kind: 'PROPERTY', name: { kind: 'STRING', value: 'a' }, value: { kind: 'STRING', value: '' } });
  });

  test('It can lex a loose sting containing special characters', () => {
    expect(lexQuery([{ start: 0, end: 16, value: 'test@example.com' }])).toEqual({
      kind: 'STRING',
      value: 'test@example.com',
    });
  });
});
