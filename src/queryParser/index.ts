import { lexQuery } from './lexer/queryLexer';
import { tokenizeQueryString } from './tokenizer/tokenizer';

export function convertQueryToAST(query: string) {
  return lexQuery(tokenizeQueryString(query));
}
