import { convertQueryToAST } from '..';

describe('convertQueryToAST', () => {
  test('It should turn a query string into an AST', () => {
    expect(convertQueryToAST('test@example.com')).toEqual({
      kind: 'STRING',
      value: 'test@example.com',
    });
  });
});
