import { QueryToken } from "./queryToken";

export function tokenizeQueryString(query: string): QueryToken[] {
  const result: QueryToken[] = [];
  let i = 0;
  // skip leading whitespace
  while (i < query.length && query[i] === " ") {
    i++;
  }
  while (i < query.length) {
    let token = { start: i, end: i + 1, value: "" };
    switch (query[i]) {
      case '"':
        {
          // string token
          let j = i + 1;
          let endOfToken = false;
          while (!endOfToken && j < query.length) {
            switch (query[j]) {
              case "\\":
                j++;
                break;
              case '"':
                endOfToken = true;
                break;
            }
            j++;
          }
          token.value = query.slice(i, j) + (endOfToken ? "" : '"');
          token.end = j;
          i = j;
        }
        break;
      case ":":
      case "-":
      case "(":
      case ")":
        {
          token.value = query[i];
          i++;
        }
        break;
      default:
        {
          // token
          let j = i;
          let endOfToken = false;
          while (!endOfToken && j < query.length) {
            switch (query[j]) {
              case ":":
              case "-":
              case "(":
              case ")":
              case '"':
              case " ":
                endOfToken = true;
                break;
              default:
                j++;
                break;
            }
          }
          token.value = query.slice(i, j);
          token.end = j;
          i = j;
        }
        break;
    }
    if (token.value === ":" && (query[i] === " " || query.length === i)) {
      result.push(token);
      token = { start: i, end: i, value: "" };
    }
    while (i < query.length && query[i] === " ") {
      // skip whitespace
      i++;
    }
    result.push(token);
  }
  return appendMissingClosingParens(result);
}

function appendMissingClosingParens(tokens: QueryToken[]): QueryToken[] {
  const result: QueryToken[] = [];
  let level = 0;
  for (let i = 0; i < tokens.length; i++) {
    switch (tokens[i].value) {
      case "(":
        result.push(tokens[i]);
        level++;
        break;
      case ")":
        level--;
        if (level >= 0) {
          result.push(tokens[i]);
        } else {
          level = 0;
        }
        break;
      default:
        result.push(tokens[i]);
        break;
    }
  }
  for (let i = 0; i < level; i++) {
    result.push({
      start: tokens[tokens.length - 1].end,
      end: tokens[tokens.length - 1].end,
      value: ")"
    });
  }
  return result;
}
