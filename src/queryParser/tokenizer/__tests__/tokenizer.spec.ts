import { tokenizeQueryString } from '../tokenizer';

describe('tokenizeQueryString', () => {
  test('It tokenizes a query string with a simple tag', () => {
    expect(tokenizeQueryString('tag:test')).toEqual([
      { start: 0, end: 3, value: 'tag' },
      { start: 3, end: 4, value: ':' },
      { start: 4, end: 8, value: 'test' },
    ]);
  });
  test('It tokenizes a query string with a simple tag followed by a space as a an empty token', () => {
    expect(tokenizeQueryString('tag: test')).toEqual([
      { start: 0, end: 3, value: 'tag' },
      { start: 3, end: 4, value: ':' },
      { start: 4, end: 4, value: '' },
      { start: 5, end: 9, value: 'test' },
    ]);
  });
  test('It tokenizes a query string with a range tag', () => {
    expect(tokenizeQueryString('age:12-23')).toEqual([
      { start: 0, end: 3, value: 'age' },
      { start: 3, end: 4, value: ':' },
      { start: 4, end: 6, value: '12' },
      { start: 6, end: 7, value: '-' },
      { start: 7, end: 9, value: '23' },
    ]);
  });
  test('It tokenizes a query string with a tag containing a string', () => {
    expect(tokenizeQueryString('foo:"a test string"')).toEqual([
      { start: 0, end: 3, value: 'foo' },
      { start: 3, end: 4, value: ':' },
      { start: 4, end: 19, value: '"a test string"' },
    ]);
  });
  test('It tokenizes a query string', () => {
    expect(tokenizeQueryString('tag:test age:12-23  foo:"a test string" bar:"test ')).toEqual([
      { start: 0, end: 3, value: 'tag' },
      { start: 3, end: 4, value: ':' },
      { start: 4, end: 8, value: 'test' },
      { start: 9, end: 12, value: 'age' },
      { start: 12, end: 13, value: ':' },
      { start: 13, end: 15, value: '12' },
      { start: 15, end: 16, value: '-' },
      { start: 16, end: 18, value: '23' },
      { start: 20, end: 23, value: 'foo' },
      { start: 23, end: 24, value: ':' },
      { start: 24, end: 39, value: '"a test string"' },
      { start: 40, end: 43, value: 'bar' },
      { start: 43, end: 44, value: ':' },
      { start: 44, end: 50, value: '"test "' },
    ]);
  });
  test('It tokenizes a complex query string', () => {
    expect(
      tokenizeQueryString('(tag:test & other:1) | ((tag:"bla test" | "test") & (range:1-20 | r:-1 | b:20.0-))')
    ).toEqual([
      { start: 0, end: 1, value: '(' },
      { start: 1, end: 4, value: 'tag' },
      { start: 4, end: 5, value: ':' },
      { start: 5, end: 9, value: 'test' },
      { start: 10, end: 11, value: '&' },
      { start: 12, end: 17, value: 'other' },
      { start: 17, end: 18, value: ':' },
      { start: 18, end: 19, value: '1' },
      { start: 19, end: 20, value: ')' },
      { start: 21, end: 22, value: '|' },
      { start: 23, end: 24, value: '(' },
      { start: 24, end: 25, value: '(' },
      { start: 25, end: 28, value: 'tag' },
      { start: 28, end: 29, value: ':' },
      { start: 29, end: 39, value: '"bla test"' },
      { start: 40, end: 41, value: '|' },
      { start: 42, end: 48, value: '"test"' },
      { start: 48, end: 49, value: ')' },
      { start: 50, end: 51, value: '&' },
      { start: 52, end: 53, value: '(' },
      { start: 53, end: 58, value: 'range' },
      { start: 58, end: 59, value: ':' },
      { start: 59, end: 60, value: '1' },
      { start: 60, end: 61, value: '-' },
      { start: 61, end: 63, value: '20' },
      { start: 64, end: 65, value: '|' },
      { start: 66, end: 67, value: 'r' },
      { start: 67, end: 68, value: ':' },
      { start: 68, end: 69, value: '-' },
      { start: 69, end: 70, value: '1' },
      { start: 71, end: 72, value: '|' },
      { start: 73, end: 74, value: 'b' },
      { start: 74, end: 75, value: ':' },
      { start: 75, end: 79, value: '20.0' },
      { start: 79, end: 80, value: '-' },
      { start: 80, end: 81, value: ')' },
      { start: 81, end: 82, value: ')' },
    ]);
  });
  test('It tokenizes a query string with a tag containing an empty string literal', () => {
    expect(tokenizeQueryString('foo:')).toEqual([
      { start: 0, end: 3, value: 'foo' },
      { start: 3, end: 4, value: ':' },
      { start: 4, end: 4, value: '' },
    ]);
  });
  test('It tokenizes a query string with an email', () => {
    expect(tokenizeQueryString('test@example.com')).toEqual([{ start: 0, end: 16, value: 'test@example.com' }]);
  });
});
