export interface QueryToken {
  start: number;
  end: number;
  value: string;
}
