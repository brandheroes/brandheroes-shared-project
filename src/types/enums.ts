export type ID_Output = string;

export enum PostMediaType {
  linkedInFeed = 'LinkedInFeed',
  instagramStory = 'InstagramStory',
  instagramFeed = 'InstagramFeed',
}

export enum GenderEnum {
  male = 'MALE',
  female = 'FEMALE',
  unknown = 'UNKNOWN',
}

export enum CampaignCommentSubject {
  general = 'GENERAL',
  objective = 'OBJECTIVE',
  audience = 'AUDIENCE',
  brief = 'BRIEF',
  review = 'REVIEW',
  rejected = 'REJECTED',
}

export enum SubmissionStatusEnum {
  pending = 'PENDING',
  rejected = 'REJECTED',
  approved = 'APPROVED',
  shared = 'SHARED',
}

export enum CampaignSubmissionProofStatusEnum {
  pending = 'PENDING',
  rejected = 'REJECTED',
  approved = 'APPROVED',
}

export enum CurrencyEnum {
  EUR = 'EUR',
  USD = 'USD',
  JPY = 'JPY',
  BGN = 'BGN',
  CZK = 'CZK',
  DKK = 'DKK',
  GBP = 'GBP',
  HUF = 'HUF',
  PLN = 'PLN',
  RON = 'RON',
  SEK = 'SEK',
  CHF = 'CHF',
  ISK = 'ISK',
  NOK = 'NOK',
  HRK = 'HRK',
  RUB = 'RUB',
  TRY = 'TRY',
  AUD = 'AUD',
  BRL = 'BRL',
  CAD = 'CAD',
  CNY = 'CNY',
  HKD = 'HKD',
  IDR = 'IDR',
  ILS = 'ILS',
  INR = 'INR',
  KRW = 'KRW',
  MXN = 'MXN',
  MYR = 'MYR',
  NZD = 'NZD',
  PHP = 'PHP',
  SGD = 'SGD',
  THB = 'THB',
  ZAR = 'ZAR',
}

export enum CampaignPublicState {
  UNDER_CONSTRUCTION = 'UNDER_CONSTRUCTION',
  PENDING_REVIEW = 'PENDING_REVIEW',
  PENDING_RELEASE = 'PENDING_RELEASE',
  ACTIVE = 'ACTIVE',
  CLOSED = 'CLOSED',
}

export enum CampaignDraftState {
  DRAFT = 'DRAFT',
  PENDING_REVIEW = 'PENDING_REVIEW',
  PROMOTED = 'PROMOTED',
  REJECTED = 'REJECTED',
}

export enum CampaignLocationType {
  COUNTRY = 'COUNTRY',
  CITY = 'CITY',
  ADDRESS = 'ADDRESS',
}

export enum InfluencerApplicationStatusEnum {
  APPLIED = 'APPLIED',
  APPROVED = 'APPROVED',
  REJECTED = 'REJECTED',
}

export enum CampaignAudienceTypeEnum {
  filter = 'FILTER',
  selected = 'SELECTED',
}

export enum CampaignTypeEnum {
  COMMISSION = 'COMMISSION',
  AFFILIATE = 'AFFILIATE',
}

export enum ZipTypeEnum {
  campaignSubmissionsImages = 'CAMPAIGN_SUBMISSIONS_IMAGES',
  campaignProofsImages = 'CAMPAIGN_PROOFS_IMAGES',
}

export enum ShopifyOrderStatus {
  PENDING = 'PENDING',
  CANCELLED = 'CANCELLED',
  APPROVED = 'APPROVED',
  FULFILLED = 'FULFILLED',
}

export enum ShopifyOrderStatusChangeEventStatus {
  APPROVED = 'APPROVED',
  DECLINED = 'DECLINED',
}
