import * as fs from 'fs';
// tslint:disable-next-line:no-implicit-dependencies
import { createSourceFile, isImportDeclaration, ScriptTarget } from 'typescript';

describe('No mock imports', () => {
  const path = './';
  const gitignore = fs.readFileSync('./.gitignore').toString();
  const files = findAllFiles(path, [
    ...gitignore
      .split('\n')
      .map((item: any) => item.trim().replace('\r', ''))
      .filter((item: any) => !item.startsWith('#') && item.length > 0)
      .map((item: any) => './' + item),
    './.git',
  ])
    .filter(file => file.endsWith('.ts'))
    .map(file => [file]);

  test('Typescript files found', async () => {
    expect(files.length).toBeGreaterThan(0);
  });

  // Files outside '__tests__' and '__mocks__' folders should not import files from '__mocks__' folders
  test.each(files)("'%s' should not import any files from '__mocks__' folders", async (file: string) => {
    const sourceFile = createSourceFile(
      'foo.ts',
      await new Promise<string>((resolve, reject) =>
        fs.readFile(file, (err: any, data: any) => (err ? reject(err) : resolve(data.toString())))
      ),
      ScriptTarget.ES5,
      true
    );
    expect(
      sourceFile.statements
        .filter(isImportDeclaration)
        .some(node => node.moduleSpecifier.getText().includes('__mocks__'))
    ).toBe(false);
  });
});

function findAllFiles(path: string, blacklist: string[]): string[] {
  return fs
    .readdirSync(path)
    .map(
      (file: any) =>
        fs.statSync(path + file).isFile()
          ? [path + file]
          : file === '__tests__' || file === '__mocks__' || blacklist.some(item => item === path + file)
            ? []
            : findAllFiles(path + file + '/', blacklist)
    )
    .reduce<string[]>((result: any, current: any) => [...result, ...current], []);
}
