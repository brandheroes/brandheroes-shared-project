import { ID_Output } from '../types/enums';
import { UserNotificationType } from './userNotificationEnums';

export type UserNotificationCampaignAction = {
  notificationType: UserNotificationType.CAMPAIGN;
  campaignId: ID_Output;
};
