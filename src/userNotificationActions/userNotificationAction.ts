import { UserNotificationCampaignAction } from './userNotificationCampaignAction';
import { UserNotificationCampaignSubmissionAction } from './userNotificationCampaignSubmissionAction';
import { UserNotificationMissingInstagramFollowerCountAction } from './userNotificationMissingInstagramFollowerCountAction';
import { UserNotificationMissingPhoneNumberAction } from './userNotificationMissingPhoneNumberAction';

export type UserNotificationAction =
  | UserNotificationCampaignAction
  | UserNotificationCampaignSubmissionAction
  | UserNotificationMissingInstagramFollowerCountAction
  | UserNotificationMissingPhoneNumberAction;
