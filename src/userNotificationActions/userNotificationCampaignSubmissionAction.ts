import { ID_Output } from '../types/enums';
import { UserNotificationType } from './userNotificationEnums';

export type UserNotificationCampaignSubmissionAction = {
  notificationType: UserNotificationType.CAMPAIGN_SUBMISSION;
  campaignSubmissionId: ID_Output;
};
