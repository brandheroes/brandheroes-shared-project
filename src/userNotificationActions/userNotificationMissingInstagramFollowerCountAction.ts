import { ID_Output } from '../types';
import { UserNotificationType } from './userNotificationEnums';

export type UserNotificationMissingInstagramFollowerCountAction = {
  notificationType: UserNotificationType.MISSING_INSTAGRAM_FOLLOWER_COUNT;
  userId: ID_Output;
};
