import { ID_Output } from '../types';
import { UserNotificationType } from './userNotificationEnums';

export type UserNotificationMissingPhoneNumberAction = {
  notificationType: UserNotificationType.MISSING_PHONE_NUMBER;
  userId: ID_Output;
};
