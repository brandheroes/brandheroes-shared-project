## I, The Reviewer, hereby pledge that the following is true
<!--- What types of changes does your code introduce? Put an `x` in all the boxes that apply: -->
- [ ] The functionality works as intended
- [ ] The testsuite is all green
- [ ] Any relevant tests have been added/removed/modified
- [ ] All proper validation has been done
- [ ] The code is easy to understand for others
- [ ] Any relevant in-code documentation has been written
- [ ] Any relevant documentation has been added to readme.md
- [ ] I accept shared responsibility for the functionality of this pull request with the author