# Shared Project

This project contains functionality shared by multiple Brandheroes projects.

# How to fetch this project as a dependency

To add this project to your code, use one of the following strategies, depending on your use case:

## Fetch a branch

This is probably the most common use case:

```
# master
yarn add git+https://bitbucket.org/brandheroes/brandheroes-shared-project.git#master

# branch with name 'feature/some-branch'
yarn add git+https://bitbucket.org/brandheroes/brandheroes-shared-project.git#feature/some-branch
```

## Fetch from a specific commit

It's possible to fetch the project based on a specfic commit hash as follows:

```
yarn add git+https://bitbucket.org/brandheroes/brandheroes-shared-project.git#<commit SHA>
```

for example

```
yarn add git+https://bitbucket.org/brandheroes/brandheroes-shared-project.git#02e6eb4f9ad3b21003c3e3b9bb998dde342089e3
```

## From a tag

```
yarn add git+https://bitbucket.org/brandheroes/brandheroes-shared-project.git#refs/tags/v0.9.6
```

# Versioning

When a new version of the library should be published, you first have to rebuild the project by running `yarn build` and committing any changes to git. Then the version number of the package should be bumped, to do this simply run `npm version <update_type>` where `<update_type>` is one of the semantic versioning release types (`patch`/`minor`/`major`), then push all your commits to bitbucket.

# CI

A push to `master` branch will

1. build
2. test
3. run npm version patch, updating package.json and tagging the commit
4. push the new tag to origin/master

A new tag will

1. build
2. test
3. run npm publish (manually triggered in Bitbucket)

As such, a succesful merge into master will automatically version, tag and publish the new version to NPM

Inspiration to push to repo from Pipelines was found here:
https://bitbucket.org/site/master/issues/13213/push-back-to-remote-from-pipelines#comment-43167296

https://npme.npmjs.com/docs/tutorials/pipelines.html - This approach didn't seem to work, publishing would fail because no user was added, i.e. the writing of ~/.npmrc fails. Trying with helper lib `ci-publish` instead.
