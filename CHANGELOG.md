**0.9.2**
Added `InfluencerApplicationStatusEnum`

**0.8.0**
Added `SHARED` status to the user submission status enum

**0.7.1**
Allowed getPropValue to iterate over an initial value property without having to give the 'value' key

**0.7.0**
A major project re-arrangement for better structure going forward. Also moved some dependencies to `devDependencies` where they belong.

**0.6.1**
Added validation for campaign radius of min 1 km and max 100 km

**0.6.0**
Removed banner/hero image on campaign
